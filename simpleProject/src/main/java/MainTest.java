import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: Alexander Pletnev
 */
public class MainTest {

    public static void main(String[] argc) {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.google.com");

        WebElement searchBox = driver.findElement(By.id("gbqfq"));
        searchBox.sendKeys("the product engine");
        searchBox.submit();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        WebElement pictureItem = driver.findElement(By.xpath("//*[@id=\"hdtb_msb\"]/div[2]/a"));
        pictureItem.click();

        List<WebElement> pictures = driver.findElements(By.className("rg_di"));
        WebElement tpeImage = pictures.get(1).findElement(By.tagName("a"));
        tpeImage.click();
        System.out.println(tpeImage.getAttribute("href"));
    }

}
