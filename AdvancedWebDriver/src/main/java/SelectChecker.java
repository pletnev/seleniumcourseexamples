import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class SelectChecker {
    public static void main(String[] argc) {

        WebDriver driver = new FirefoxDriver();
        driver.get("file:///projects/seleniumCourse/AdvancedWebDriver/resources/Select.html");

        Select selectBox = new Select(driver.findElement(By.name("hero")));
        WebElement defaultElement = selectBox.getFirstSelectedOption();
        System.out.println(defaultElement.getText());
        selectBox.selectByVisibleText("Чебурашка");
//        WebElement selectBox = driver.findElement(By.name("hero"));
//        System.out.println(defaultOption.getAttribute("value"));

    }
}
