import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class TableChecker {
    public static void main(String[] argc) {
        WebDriver driver = new FirefoxDriver();
        driver.get("file:///projects/seleniumCourse/AdvancedWebDriver/resources/Table.html");


        WebElement value = driver.findElement(By.xpath("/html/body/table/tbody/tr/td[2]/table/tbody/tr[1]/td"));
        System.out.println(value.getText());

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOf(value));

        System.out.println(element.getText());


    }
}
