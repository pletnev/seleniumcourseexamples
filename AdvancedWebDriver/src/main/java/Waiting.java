import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class Waiting {

    public static void main(String[] argc) {


        WebDriver driver = new FirefoxDriver();
        driver.get("http://google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("the product engine");
        searchBox.submit();
        WebDriverWait driverWait = new WebDriverWait(driver, 5);
        WebElement imageLink = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"hdtb_msb\"]/div[2]/a")));
        imageLink.click();

        List<WebElement> pictures = driver.findElements(By.className("rg_di"));
        WebElement tpeImage = pictures.get(2).findElement(By.tagName("a"));
        tpeImage.click();
        System.out.println(tpeImage.getAttribute("href"));
    }

}
