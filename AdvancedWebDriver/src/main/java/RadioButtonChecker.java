import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class RadioButtonChecker {
    public static void main(String[] argc) {

        WebDriver driver = new FirefoxDriver();
        driver.get("file:///projects/seleniumCourse/AdvancedWebDriver/resources/RadioButtons.html");

        List<WebElement> radioElements = driver.findElements(By.name("group"));
        System.out.println(radioElements.size());
        System.out.println(radioElements.get(0).getAttribute("value"));
        radioElements.get(radioElements.size() - 1).click();

        for (WebElement radio : radioElements) {
            if (radio.isSelected()) {
                System.out.println(radio.getAttribute("value"));
            }
        }



    }
}
