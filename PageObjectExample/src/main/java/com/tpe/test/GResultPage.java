package com.tpe.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class GResultPage {

    private final WebDriver driver;

    public GResultPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getFirstLink() {
        return driver.findElement(By.xpath("//*[@id=\"rso\"]//a[1]")).getText();
    }
}
