package com.tpe.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class GHomePage {
    private final WebDriver driver;

    public GHomePage(FirefoxDriver driver) {
        this.driver = driver;
    }

    public GResultPage search(String searchText) {
        driver.findElement(By.name("q")).sendKeys(searchText);
        return new GResultPage(this.driver);
    }
}
