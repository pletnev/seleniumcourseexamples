import com.tpe.test.GHomePage;
import com.tpe.test.GResultPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * @author: <<a href="mailto:apletnev@productengine.com">Alexander Pletnev</a>
 */
public class GoogleIndexPage {

    private FirefoxDriver driver;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get("http://www.google.com/ncr");
    }

    @After
    public void tearDown() throws Exception {
        driver.close();
    }

    @Test
    public void testSearch() throws Exception {
        GHomePage home = new GHomePage(driver);
        GResultPage result = home.search("The Product Engine");
        Assert.assertTrue(result.getFirstLink().contains("Engine"));
    }

    @Test
    public void testSearchWiki() throws Exception {
        GHomePage home = new GHomePage(driver);
        GResultPage result = home.search("Wikipedia");
        Assert.assertTrue(result.getFirstLink().contains("Engine"));
    }
}
